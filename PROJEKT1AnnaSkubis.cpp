/// ------------------------ #define 
#define ILEROZMIAROW 5 // Jak dużo opcji co do wielkości tablic - 5. 
#define _CRT_SECURE_NO_WARNINGS

/// ------------------------ #include
#include <iostream>
#include<ctime>
#include<fstream>
#include<string>
#include<vector>

using namespace std;

void Tworzenie_liczb(char mode)
{
    fstream plik;
    if (mode=='a')
    {
        string nazwy[] = { "10000","50000" ,"100000" ,"500000","1000000" };
        for (int i = 0; i < ILEROZMIAROW; i++)
        {
            plik.open("Losowe_dane_dla" + nazwy[i] + ".txt", ios::out);
            int wielkosci[] = {10000,50000,100000,500000,1000000 };
            for (int k = 0; k < 100; k++)
            {
                plik << endl << k << endl;                    /*Utowrzone zostaje 100tablic, o danej wielkości
                                                              (10 000, 50 000 itd.), którą uzyskuje 
                                                              z tablicy wielkosci[] */
                for (int j = 0; j < wielkosci[i]; j++)
                {
                    plik << rand() << " ";
                }
            }
            plik.close();
        }
    }
    else if (mode == 't')
    {                                                         /* Dla testowania tworzę 100 tablic o wielkości 10 */
        plik.open("testowanie.txt", ios::out);
        for (int k = 0; k < 100; k++)
        {
            plik << endl << k << endl;
            for (int j = 0; j < 10; j++)
            {
                plik << rand() << " ";
            }
        }
    }
    plik.close();

}
                                                               /* Funkcji wyświetl muszę przekazać tablicę, 
                                                               a ta następnie będzie element po elemencie wypisywać
                                                               kolejne wartości */
void wyswietl(vector<int> tablica)
{
    for (int i = 0; i < tablica.size(); i++)
    {
        cout << tablica[i] << " ";
    }
}
                                                               /* FUNKCJA WCZYTAJ -----------
                                                               Funkcja która ma za zadanie wczytanie do trójwymiarowej
                                                               tablicy, która muszę jej przekazać (tablice_danych)
                                                               losowych danych, które muszą być wcześniej stworzone
                                                               przy pomocy funkcji Tworzenie_liczb */

void wczytaj(vector < vector< vector <int>>>& tablice_danych, char mode)
{
    fstream plik;
    int numer_tablicy;
    if (mode == 'a')
    {
        string nazwy[] = { "10000","50000" ,"100000" ,"500000","1000000" };
        int wielkosci[] = {10000,50000,100000,500000,1000000};
        tablice_danych.resize(ILEROZMIAROW);

        for (int i = 0; i < ILEROZMIAROW; i++)
        {
            plik.open("Losowe_dane_dla" + nazwy[i] + ".txt", ios::in);

            if (!plik.is_open())
            {cout << "Nie udało się otworzyć pliku z wygenerowanymi liczbami" << endl; exit;}

            tablice_danych[i].resize(100);
            for (int k = 0; k < 100; k++)
            {
                plik >> numer_tablicy;
                tablice_danych[i][k].resize(wielkosci[i]);
                for (int j = 0; j < wielkosci[i]; j++)
                {
                    plik >> tablice_danych[i][k][j];
                }
            }
            plik.close();
        }
    }

    else if (mode == 't')
    {
        plik.open("testowanie.txt", ios::in);
        tablice_danych.resize(1);
        tablice_danych[0].resize(100);
        for (int k = 0; k < 100; k++)
        {
            plik >> numer_tablicy;
            tablice_danych[0][k].resize(10);
            for (int j = 0; j < 10; j++)
            {
                plik >> tablice_danych[0][k][j];
            }
        }
    }
}

                                                            /* --- SORTOWANIE SZYBKIE - QUICKSORT -----
                                                            --- dzięki funkcji sortowanie_szybkie, przypiszę
                                                            tablicy_danych wartości 
                                                            --- metoda sortowanie to docelowe sortowanie tablicy 
                                                            (jednej po x elementów)
                                                            --- */
class sortowanie_szybkie
{
    vector <int> tablice_danych;
public:
    sortowanie_szybkie() {}; 
    sortowanie_szybkie(vector <int> tablice_danych);
    void sortowanie(int lewy, int prawy);
    void sortowanie_odw(int lewy, int prawy);
    vector <int> get() { return tablice_danych;}
};

sortowanie_szybkie::sortowanie_szybkie(vector <int> tablice_danych)
{this->tablice_danych = tablice_danych;}

void sortowanie_szybkie::sortowanie(int lewy, int prawy)
{
    int i = (lewy + prawy) / 2; 
    int piwot = tablice_danych[i];
    tablice_danych[i] = tablice_danych[prawy];           // Zamiana piwotu z ostatnim elmentem tablicy. 
    int j = lewy;
    for (i = lewy; i < prawy; i++)
    {
        if (tablice_danych[i] < piwot)
        {
            swap(tablice_danych[i], tablice_danych[j]);
            j++;
        }
    }
    tablice_danych[prawy] = tablice_danych[j];            // Wymieniamy piwot z elementem na pozycji j-tej. 
    tablice_danych[j] = piwot;                            // środkowy element ustawiamy na piwot. Podział na dwie partycje zakończony. 
    if (lewy < j - 1)
        sortowanie(lewy, j - 1);
    if (prawy > j + 1)
        sortowanie(j + 1, prawy);
}

void sortowanie_szybkie::sortowanie_odw(int lewy, int prawy)
{
    int i = (lewy + prawy) / 2;
    int piwot = tablice_danych[i];
    tablice_danych[i] = tablice_danych[prawy];
    int j = lewy;
    for (i = lewy; i < prawy; i++)
    {
        if (tablice_danych[i] > piwot)                      // Jedyna różnica 
        {
            swap(tablice_danych[i], tablice_danych[j]);
            j++;
        }
    }
    tablice_danych[prawy] = tablice_danych[j];
    tablice_danych[j] = piwot;
    if (lewy < j - 1)
        sortowanie(lewy, j - 1);
    if (prawy > j + 1)
        sortowanie(j + 1, prawy);
}

class sortowanie_scalanie
{
    vector <int> tablice_danych;
    vector <int> tablice_pomocnicza;
public:
    sortowanie_scalanie() {};
    sortowanie_scalanie(vector <int> tablice_danych);
    void sortowanie(int lewy, int prawy);
    vector <int> get() { return tablice_danych; }
};

sortowanie_scalanie::sortowanie_scalanie(vector <int> tablice_danych)
{
    this->tablice_danych = tablice_danych;
    tablice_pomocnicza.resize(tablice_danych.size());
}
void sortowanie_scalanie::sortowanie(int lewy, int prawy)
{
    int srodek, i, i1, i2;
    srodek = (prawy + lewy + 1) / 2;
    if (srodek - lewy > 1) sortowanie(lewy, srodek - 1);
    if (prawy - srodek > 0) sortowanie(srodek, prawy - 1);

    i1 = lewy;
    i2 = srodek;
    for (i = lewy; i <= prawy; i++)
    {
        if ((i1 == srodek) || ((i2 <= prawy) && (tablice_danych[i1] > tablice_danych[i2])))
        {
            tablice_pomocnicza[i] = tablice_danych[i2++];
        }
        else
        {
            tablice_pomocnicza[i] = tablice_danych[i1++];
        }
    }
    for (i = lewy; i <= prawy; i++)
    {
        tablice_danych[i] = tablice_pomocnicza[i];
    }
}


/// ///////////////////////////////////////////////////////////////////////////

class sortowanie_kopcowanie
{
    vector <int> tablice_danych;
public:
    sortowanie_kopcowanie() {};
    sortowanie_kopcowanie(vector <int> tablice_danych);
    void sortowanie();
    vector <int> get() { return tablice_danych; }
};

sortowanie_kopcowanie::sortowanie_kopcowanie(vector <int> tablice_danych)
{
 
    this->tablice_danych = tablice_danych;
}
void sortowanie_kopcowanie::sortowanie()
{
    int k, j, pom, l;
    for (int i = 2; i <= tablice_danych.size() - 1; i++)
    {
        j = i;
        k = j / 2;
        pom = tablice_danych[i];
        while ((k > 0) && (tablice_danych[k] < pom))
        {
            tablice_danych[j] = tablice_danych[k];
            j = k;
            k = j / 2;
        }
        tablice_danych[j] = pom;
    }


    for (int i = tablice_danych.size() - 1; i > 1; i--)
    {
        swap(tablice_danych[1], tablice_danych[i]);
        j = 1;
        k = 2;
        while (k < i)
        {
            if ((k + 1 < i) && (tablice_danych[k + 1] > tablice_danych[k]))
            l = k + 1;
            else
            l = k;
            if (tablice_danych[l] <= tablice_danych[j]) break;
            swap(tablice_danych[j], tablice_danych[l]);
            j = l;
            k = j + j;
        }
    }
}

bool test(vector<int> elementy)
{
    for (int i = 0; i < elementy.size() - 1; i++)
    {
        if (elementy[i] > elementy[i + 1])
        {
            return false;
        }
    }
    return true;
}

int main()
{

    srand(time(NULL));
    time_t czas;
    struct tm* ptr;
    time(&czas);
    ptr = localtime(&czas);
    char* data = asctime(ptr);

                                  /* Stworzenie zmiennej plikowej */
    fstream plik;

                                  /* Trójwymiarowa tablica danych */
    vector < vector< vector <int>>> tablice_danych;
    vector <int>tablice_pomocnicza;


    /*JEŚLI TRZEBA ZNOWU WYGENEROWAC TO ODKOMENTOWAC */
    Tworzenie_liczb('a');

    clock_t suma = 0;

    wczytaj(tablice_danych, 'a');
    plik.open("czasy.txt", ios::app);
                                       /*Wpisz aktualną datę do pliku*/
    plik << data << endl;
    char znak;
    double procent;
    while (1)
    {
        suma = 0;
        cout << "Wybierz jakie sortowanie chcesz wykonać. Wpisz:" << endl << "q - quicksort" << endl << "s  - przez scalanie" << endl << "k - przez kopcowanie" << endl;
        cin >> znak;
        cout << "Ile procent tablicy ma być już posortowane? Wpisz liczbę. (100 oznacza posortowaną już odwrotnie tablicę)";
        cin >> procent;
        switch (znak)
        {
        case 'q':

            for (int i = 0; i < ILEROZMIAROW; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    if (procent > 0 && procent != 100)
                    {
                        /* Tworzymy obiekt klasy sortowowanie_szybkie */
                        sortowanie_szybkie sort(tablice_danych[i][j]);

                        /* Weź po uwagę tylko pierwsze x procent elementów i posortuj*/
                        sort.sortowanie(0, (tablice_danych[i][j].size() * procent / 100) - 1);

                        /* Do tablicy pomocniczej przypisz częsciowo posortowaną już tablice.*/
                        tablice_pomocnicza = sort.get();
                    }
                    else if (procent == 100)
                    {
                        sortowanie_szybkie sort(tablice_danych[i][j]);
                        sort.sortowanie_odw(0, (tablice_danych[i][j].size()) - 1);
                        tablice_pomocnicza = sort.get();
                    }
                    else
                    {
                        tablice_pomocnicza = tablice_danych[i][j];
                    }


                    sortowanie_szybkie sort(tablice_pomocnicza);

                  
                    clock_t start = clock();
                                                                      /*Docelowe sortowanie*/
                    sort.sortowanie(0, tablice_pomocnicza.size() - 1);
                
                    suma += clock() - start;
                }
                plik << suma / 100 << endl;
            }
            break;
        case 's':
            for (int i = 0; i < ILEROZMIAROW; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    if (procent > 0 && procent != 100)
                    {
                        sortowanie_szybkie sort(tablice_danych[i][j]);
                        sort.sortowanie(0, (tablice_danych[i][j].size() * procent / 100) - 1);
                        tablice_pomocnicza = sort.get();
                    }
                    else if (procent == 100)
                    {
                        sortowanie_szybkie sort(tablice_danych[i][j]);
                        sort.sortowanie_odw(0, (tablice_danych[i][j].size()) - 1);
                        tablice_pomocnicza = sort.get();
                    }
                    else
                    {
                        tablice_pomocnicza = tablice_danych[i][j];
                    }
                    sortowanie_scalanie sort(tablice_pomocnicza);
                    clock_t start = clock();
                    sort.sortowanie(0, tablice_pomocnicza.size() - 1);
                    suma += clock() - start;
                }

                plik << suma / 100 << endl;
            }
            break;
        case 'k':
            plik << data << endl;
            for (int i = 0; i < ILEROZMIAROW; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    if (procent > 0 && procent != 100)
                    {
                        sortowanie_szybkie sort(tablice_danych[i][j]);
                        sort.sortowanie(0, tablice_danych[i][j].size() * procent / 100 - 1);
                        tablice_pomocnicza = sort.get();
                    }
                    else if (procent == 100)
                    {
                        sortowanie_szybkie sort(tablice_danych[i][j]);
                        sort.sortowanie_odw(0, tablice_danych[i][j].size() - 1);
                        tablice_pomocnicza = sort.get();
                    }
                    else {
                        tablice_pomocnicza = tablice_danych[i][j];
                    }
                    sortowanie_kopcowanie sort(tablice_pomocnicza);
                    clock_t start = clock();
                    sort.sortowanie();
                    suma += clock() - start;

                }
                plik << suma / 100 << endl; 
            }
            break;
            default:
            break;
        }
    }
    plik.close();
}
